---
version: 1
---

# CocoAd API GetOffer

---

1. #### General Purpose

   > This guiding document is to describe Server2Server API intergration with Coconut Ad system for publishers. Publisher server will use the interface as described, using HTTP GET requesting advertisement data from Coconut Ad system.
   >
   > It is advised to send request to Coconut Ad system every 15 minutes to refresh for latest offer data.
2. #### Integration Process

   1. Please contact with Coconut Ad Account managers to setup account. Obtain API Token from represenatives.
   2. publisher will obtain advertisment details through this API.
3. ##### GetOffers API

   1. **Endpoint**
      > [https://api.cocoad.mobi/v1/getoffers?token={your token}](https://api.cocoad.mobi/v1/getoffers?token={your token})
   2. **Request Parameters**
      | Parameter | Mandatory | Description |
      | :--- | :--- | :--- |
      | token | true | obtain offer data through this token |
      | platform | false | options: android/ios, return all offers when omitted |
   3. ##### Response

      | Field Name | Type | Description |
      | :--- | :--- | :--- |
      | status | string | OK \| Fail |
      | msg | string | empty if no errors, otherwise will be the reason of failure |
      | total\_count | integer | the number of offers that API returned |
      | offers | array | offer collection, single offer details will be shown below. |
   4. | Field Name | Type | Mandatory | Description |
      | :--- | :--- | :--- | :--- |
      | id | string | true | offer unique id |
      | name | string | true | offer name |
      | pkg\_name | string | true | offer package name, ie: com.beauty.hdflash for Android,  1229635195 for iOS |
      | pkg\_size | string | true | package size, if empty indicate don't have size, or will be something like "12.3M" |
      | platform | string | true | Android/iOS for now |
      | preview\_url | string | true | preview url for this offer, It's official url getting from GooglePlay or Apple iTunes. |
      | icon\_url | string | true | offer icon image url |
      | os\_min\_version | string | true | minimum os version required for this offer to run. |
      | countries | array | true | restricted countries for this offer to run on. ISO-3166-2 standard country code，ex: {"0":"CN","1":"US"} |
      | currency | string | true | default: USD |
      | daily\_cap | integer | true | daily valid amount of conversions for this offer will be paid, this number could be changed at every request. |
      | payout | float | true | offer price |
      | payout\_model | string | true | default: CPA |
      | traffic\_type | string | true | non-incentive, incentive or both if empty |
      | creatives | array | false | all creative files details for this offer |
      | impression\_url | string | true | tracking impression url for this offer |
      | click\_url | string | true | tracking click url for this offer |
      | launched\_at | datetime | true | offer launch time: {"date":"2018-01-22 00:00:00.000000","timezone\_type":3,"timezone":"UTC"} |
      | expired\_at | datetime | true | offer expired time: {"date":"2018-01-22 00:00:00.000000","timezone\_type":3,"timezone":"UTC"} |
      | description | string | true | offer description |
      | need\_gaid\_idfa | boolean | true | true/false, indicate that wether this offer needs gaid/idfa or not. |
4. ##### Tracking Link Macro list

   **click tracking url:**  
   example: [https://track.cocoad.mobi/click?a=436877&o=0FSNSP15QQ&tid=\_{AFF\_CLICK\_ID}\_⊂\_affid=\_{SUB\_AFFID}\_&google\_gaid=\_{GOOGLE\_GAID}\_&ios\_idfa={IOS\_IDFA\_}](https://track.cocoad.mobi/click?a=436877&o=0FSNSP15QQ&tid=_{AFF_CLICK_ID}_&sub_affid=_{SUB_AFFID}_&google_gaid=_{GOOGLE_GAID}_&ios_idfa={IOS_IDFA_})

   | Macro | Mandatory | Descrtiption |
   | :--- | :--- | :--- |
   | {AFF\_CLICK\_ID} | true | unique click id, must filled by publisher side |
   | {SUB\_AFFID} | true | sub publisher id of publisher |
   | {GOOGLE\_GAID} | true | if user device is based on android platform |
   | {IOS\_IDFA} | true | if user device is based on iOS platform |
   | {CLICK\_TIME} | false | the time user clicks publisher click url, format: unix timestamp |
   | {IP} | false | user ip when click happened, if publisher uses server click. |
   | {UA} | false | user agent if publisher uses server click. |
   | {DEVICE\_OS} | false | user device os \(Android/iOS\) if publisher uses server click |
   | {DEVICE\_OS\_VER} | false | user device os version if publisher uses server click |
   | S1 ~ S5 | false | placeholders |

5. ##### Conversion Postback

   Endpoint: provided by publisher  
   ask AM to setup on our platform.

_**Creatives example:**_

`"creatives": {                
    "0":{"url":"https://img.cocoad.mobi/upload/creative/2018/02/19/08d6cea6cadff31e360e592fd43e8287.png","type":"native","height":0,"width":0},                
    "1":{"url":"https://img.cocoad.mobi/upload/creative/2017/11/17/a4a944d268b1d73dbc85f49630c84422.jpeg","type":"native","height":0,"width":0},                
    "2":{"url":"https://img.cocoad.mobi/upload/creative/2018/02/19/09270c8e6810812ac6f058963c59a1e4.png","type":"native","height":0,"width":0},                
    "3":{"url":"https://img.cocoad.mobi/upload/creative/2018/02/18/1c50f03bc128c9b02c0bd9543808ee9b.png","type":"native","height":0,"width":0},                
    "4":{"url":"https://img.cocoad.mobi/upload/creative/2018/02/18/bee3a21fb1563e0dded41b93df3a450b.png","type":"native","height":0,"width":0},                
    "5":{"url":"https://img.cocoad.mobi/upload/creative/2018/02/19/fb7dfdb6f4015b255f7e09c1d3ca19ce.png","type":"native","height":0,"width":0},                
    "6":{"url":"https://img.cocoad.mobi/upload/creative/2018/02/18/f777fe6395b663c268b900d83ff0b2a5.png","type":"native","height":0,"width":0}                
}`

