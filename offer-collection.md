# Offer Collection

---

## Design Thought

##### 关于 offer 表的设计：

> 每个广告主的账号都有一个更新频率，每次更新会与上次更新比对，如果价格、地区等重要信息发生变更则需要记录，如果缺失则需要将该 offer 标记为下线。
>
> 如果将 offer 状态以及容易发生变更的字段都保存在一起，每次都对数据库进行 update 操作，会造成效率低下，易出错，追踪历史很难等等情况出现。
>
> 目前的设计思路是将变化的部分提取到另外一张表\(Target\)，每次更新对于 target 表来说都是一次 insert 操作，offer 表与 target 表一对一关联则只关联最后一次的target记录；每次的插入记录也可视为 offer 信息变更的历史，另起一个定时任务清除多余的 target 表记录。



