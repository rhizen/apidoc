# Conversion Tracking

---

**Tracking Url: ** [https://track.cocoad.mobi/conversion?source={your advertiser}&tid={TID}&x-token={cocoad provide if needed}](https://track.cocoad.mobi/conversion?source={your advertiser}&tid={TID}&x-token={cocoad provide if needed} "https://track.cocoad.mobi/conversion?source={your advertiser}&amp;tid={TID}&amp;x-token={cocoad provide if needed}")

### Parameter explain:

* source: 广告主 id，由 cocoad 指定
* tid: 点击时发生的 transaction id，全局唯一，用来判断由哪个流量带来的转化
* x-token: 由 cocoad 颁发的唯一验证码，防止流量端作弊，广告主需要妥善保存防止泄露。



